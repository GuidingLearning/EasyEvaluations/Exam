#!/usr/bin/perl

# Given as a parameter the name (without the extension) of a latex
# file "foo1" which starts by a line of the format
# "\documentclass[foo1,foo2,foo3]{foo}"

# Creates similar latex files "foo1_tas.tex", "foo1_students.tex" and
# "foo1_solution.tex", but starting with a line of format
# "\documentclass[solution,markingScheme]{foo}",
# "\documentclass{project}" and "\documentclass[solution]{foo}",
# compile them and produce the correspondinf postscript files.

use strict;

if ( @ARGV != 1 ) {
    die("Usage: $0 <assignment_or_project_file.tex>\n");
}

my $fname = $ARGV[0];
my $line;

my $tas = "_tas";
my $st = "_students";
my $sol = "_solution";



$fname =~ s/\.tex$//;

open (FILE, "$fname.tex") or die ("Cannot open file $fname.tex");
open (FILE_TA, ">${fname}${tas}.tex") or die ("Cannot open file ${fname}${tas}.tex");
open (FILE_ST, ">${fname}${st}.tex") or die ("Cannot open file ${fname}${st}.tex");
open (FILE_SOL, ">${fname}${sol}.tex") or die ("Cannot open file ${fname}${sol}.tex");
while ($line = <FILE>)
  {
      if ($line =~ m/^\\documentclass.*\{(\w+)\}/)
      {
	  print FILE_TA "\\documentclass\[solution,suggestedMarkingScheme,markingScheme,authorship,filename\]\{$1\}\n";
	  print FILE_ST "\\documentclass\[spaceForAnswer,markingScheme\]\{$1\}\n";
	  print FILE_SOL "\\documentclass\[solution,markingScheme\]\{$1\}\n";
      } 
      else
      {
	  print FILE_TA $line;
	  print FILE_ST $line;
	  print FILE_SOL $line;
      }
  }

close (FILE);
close (FILE_TA);
close (FILE_ST);
close (FILE_SOL);

print "** Compiling ${fname}${tas}.tex\n";
system("latex ${fname}${tas}.tex && latex ${fname}${tas}.tex && latex ${fname}${tas}.tex && dvips ${fname}${tas}.dvi -o ${fname}${tas}.ps && dvips ${fname}${tas}.dvi -o ${fname}${tas}-untagged.ps && psmark -i ${fname}${tas}-untagged.ps -o ${fname}${tas}.ps -s 90 -r 55 -b 0.9 'DO NOT DISTRIBUTE' && ps2pdf ${fname}${tas}.ps");

print "** Compiling ${fname}${st}.tex\n";
system("latex ${fname}${st}.tex && latex ${fname}${st}.tex && latex ${fname}${st}.tex && dvips ${fname}${st}.dvi -o ${fname}${st}.ps && ps2pdf ${fname}${st}.ps");

print "Compiling ${fname}${sol}.tex\n";
system("latex ${fname}${sol}.tex && latex ${fname}${sol}.tex && latex ${fname}${sol}.tex && dvips ${fname}${sol}.dvi -o ${fname}${sol}.ps && dvips ${fname}${sol}.dvi -o ${fname}${sol}-untagged.ps && psmark -i ${fname}${sol}-untagged.ps -o ${fname}${sol}.ps -s 90 -r 55 -b 0.9 'DO NOT DISTRIBUTE' && ps2pdf ${fname}${sol}.ps");

print "Erasing non-necessary generated files.\n";
system("rm -f ${fname}_*.tex; rm -f ${fname}_*.aux; rm -f ${fname}_*.log; rm -f ${fname}_*.dvi;rm -f ${fname}_*.toc;");
